//
//  WeatherModel.swift
//  TemperatureControl
//
//  Copyright © 2019 Андрей Мироненко. All rights reserved.
//

import Foundation
import Alamofire

class WeatherModel {
    
    // MARK: Static Properties
    
    static let shared = WeatherModel()
    
    func getCurrentWether(longitude: Double,
                          latitude: Double) {
        let request: DataRequest = SessionManager.shared.sessionManager.request(APIMainDataVersionWeatherURL,
                                                                                method: .get,
                                                                                parameters: ["lat"   : latitude,
                                                                                             "lon"   : longitude,
                                                                                             "appid" : APIKey])
        
        if debugPrintRequest == true {
            debugPrint(request)
        }
        
        // Use automatic validation
        
        request.validate()

        // JSON response needed
        request.responseJSON { (response) in
            // Debuging
            
            if printResponceTimelineInfo == true {
                print("Timeline info for responce of \"\(APIMainDataVersionWeatherURL.absoluteString)\": \(response.timeline)")
            }
            if printResponceMetricsInfo == true {
                print("Metrics info for response of \"\(APIMainDataVersionWeatherURL.absoluteString)\": \(String(describing: response.metrics))")
            }
            
            if debugPrintResponse == true {
                debugPrint(response)
            }
            
            var message: String = "Some API errors appears"
            
            // Check reponse result
            switch response.result {
            case .success:
                // Try to make JSON array
                if let json: [String : AnyObject] = response.result.value as? [String : AnyObject] {
                    if let cod: Int = json["cod"] as? Int,
                        cod == 200 {
                    
                        if let mainJSONPart: [String : AnyObject] = json["main"] as? [String : AnyObject],
                            let temp: Double = mainJSONPart["temp"] as? Double,
                            let location: String = json["name"] as? String,
                            let weatherJSONPart: [AnyObject] = json["weather"] as? [AnyObject],
                            let weatherFirstJSONPart: [String : AnyObject] = weatherJSONPart.first as? [String : AnyObject],
                            let clouds: String = weatherFirstJSONPart["description"] as? String {
                            if  let rainJSONPart: [String : AnyObject] = json["rain"] as? [String : AnyObject],
                                let rain: Double = rainJSONPart["3h"] as? Double{
                                self.sendWeatherNotification(with    : true,
                                                             message : "",
                                                             clouds  : clouds,
                                                             rain    : rain,
                                                             temp    : temp,
                                                             location: location)
                          
                            print("rain \(rain)")
                            } else {
                            print("temp \(temp)")
                            print("clouds \(clouds)")
                            
                            
                            self.sendWeatherNotification(with    : true,
                                                         message : "",
                                                         clouds  : clouds,
                                                         rain    : 0,
                                                         temp    : temp,
                                                         location: location)
                            }
                        } else {
                            message = "Some error appears with JSON parsing"
                            self.sendWeatherNotification(with    : false,
                                                         message : message,
                                                         clouds  : "Undefined",
                                                         rain    : 0.0,
                                                         temp    : 0.0,
                                                         location: "error")
                        }
                    }
                } else {
                    self.sendWeatherNotification(with    : false,
                                                 message : message,
                                                 clouds  : "Undefined",
                                                 rain    : 0.0,
                                                 temp    : 0.0,
                                                 location: "error")
                }
            case .failure(let error):
                self.sendWeatherNotification(with    : false,
                                             message : message,
                                             clouds  : "Undefined",
                                             rain    : 0.0,
                                             temp    : 0.0,
                                             location: "error")
                print(error)
            }
        }
    }
    
    // MARK: Private Methods
    
    private func sendWeatherNotification(with result : Bool,
                                         message     : String,
                                         clouds      : String,
                                         rain        : Double,
                                         temp        : Double,
                                         location    : String) {
        NotificationCenter.default.post(name     : Notification.Name.weatherResult,
                                        object   : nil,
                                        userInfo : ["result"  : result,
                                                    "message" : message,
                                                    "clouds"  : clouds,
                                                    "rain"    : rain,
                                                    "temp"    : temp,
                                                    "location": location])
    }
}
