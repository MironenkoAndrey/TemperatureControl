//
//  Constants.swift
//  TemperatureControl
//
//  Copyright © 2019 Андрей Мироненко. All rights reserved.
//

import Foundation

// Kelvin
let kelvin = 273.15

// API Debuging
let printResponceTimelineInfo : Bool = true
let printResponceMetricsInfo  : Bool = true
let debugPrintRequest         : Bool = true
let debugPrintResponse        : Bool = true

// API Key
let APIKey: String = "c12535f2980191a8d80b00bb531e960f"

// URLs
let APIMainURL: URL = URL(string: "https://api.openweathermap.org")!
let APIMainDataURL: URL = URL(string: "\(APIMainURL.absoluteString)/data")!
let APIMainDataVersionURL: URL = URL(string: "\(APIMainDataURL.absoluteString)/2.5")!
let APIMainDataVersionWeatherURL: URL = URL(string: "\(APIMainDataVersionURL.absoluteString)/weather")!

// Notifications
extension Notification.Name {
    // Used for Model -> Manager communication
    static let weatherResult   = NSNotification.Name("weatherResult")

    // Used for Manager -> ViewController communication
    static let weatherProcessed   = NSNotification.Name("weatherProcessed")
}
