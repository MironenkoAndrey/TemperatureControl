//
//  ViewController.swift
//  TemperatureControl
//
//  Copyright © 2019 Андрей Мироненко. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: IBOutlet Properties
    
    @IBOutlet var weatherLabel: UILabel!
    @IBOutlet weak var skyLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    // MARK: Override Mthods
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        LocationManager.shared.manager.requestWhenInUseAuthorization()
        
        // Subscribe for notifications
        NotificationCenter.default.addObserver(self,
                                               selector : #selector(weatherProcessed(_ :)),
                                               name     : Notification.Name.weatherProcessed,
                                               object   : nil)
        
        // Get current weather
    

        
        WeatherManager.shared.getWeatherForCurrentLocation()
        
        var date1 = Date().timeIntervalSince1970
//        if UserDefaults.standard.value(forKey: "date") < date1 {
//            print(date1)
//        }
    
        
        if let weatherString: String = UserDefaults.standard.string(forKey: "weather") {
            weatherLabel.text = weatherString
        }
        if let skyString: String = UserDefaults.standard.string(forKey: "sky") {
            skyLabel.text = skyString
        }
        if let rainString: String = UserDefaults.standard.string(forKey: "rain") {
            rainLabel.text = rainString
        }
        if let locationString: String = UserDefaults.standard.string(forKey: "location") {
            locationLabel.text = locationString
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // Remove subscription for notifications
        NotificationCenter.default.removeObserver(self,
                                                  name: Notification.Name.weatherProcessed,
                                                  object: nil)
        
        super.viewWillDisappear(animated)
    }

  
    // MARK: Private Methods

    @objc private func weatherProcessed(_ notification: Notification) {
        if let temp = WeatherManager.shared.currentTemp{
            if let clouds = WeatherManager.shared.currentClouds{
                let  currentTemp = NSString(format: "%.2f", temp)
                weatherLabel.text = "Weather now : \(currentTemp) °C"
                skyLabel.text = "Sky now : \(clouds)"
                locationLabel.text = "Location : \(WeatherManager.shared.currentLocation!)"
                if let rain = WeatherManager.shared.currentRain{
                    if rain == 0.0 {
                        rainLabel.text = "No rain"
                    } else {
                        rainLabel.text = "Rain volume for the last 3 hours: \(rain)"
                    }
                }
  
                
                else {
                    weatherLabel.text = "Weather now is 'Unknown' °C"
                }
            }
        }
        
            UserDefaults.standard.set(Date.self, forKey: "date")

            UserDefaults.standard.set(weatherLabel.text!, forKey: "weather")
      
            UserDefaults.standard.set(skyLabel.text!, forKey: "sky")
        
            UserDefaults.standard.set(rainLabel.text!, forKey: "rain")

            UserDefaults.standard.set(locationLabel.text!, forKey: "location")
        
        
   
    }
}

