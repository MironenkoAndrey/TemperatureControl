//
//  WeatherManager.swift
//  TemperatureControl
//
//  Copyright © 2019 Андрей Мироненко. All rights reserved.
//

import Foundation
import CoreLocation

class WeatherManager: NSObject {
    
    // MARK: Static Properties
    
    static let shared = WeatherManager()
    
    // MARK: Public Properties
    
    var currentTemp: Double?
    var currentClouds: String?
    var currentRain: Double?
    var currentLocation: String?
    
    // MARK: Public methods
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self,
                                               selector : #selector(weatherResult(_ :)),
                                               name     : Notification.Name.weatherResult,
                                               object   : nil)
        LocationManager.shared.manager.delegate = self
        
    }

    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: Notification.Name.weatherResult,
                                                  object: nil)
    }
    
    func getWeatherForCurrentLocation() {
        LocationManager.shared.manager.requestLocation()
    }
    
    @objc private func weatherResult(_ notification: Notification) {
        if let result = notification.userInfo?["result"] as? Bool,
            result == true,
            let clouds: String = notification.userInfo?["clouds"] as? String,
            let location: String = notification.userInfo?["location"] as? String,
            let temp = notification.userInfo?["temp"] as? Double {
            if clouds == "Sky is Clear"{
                currentClouds = "☀️"
            } else if clouds == "broken clouds"{
                currentClouds = "☁️"
            } else if clouds == "overcast clouds"{
                 currentClouds = "☁️☁️"
            } else {
                currentClouds = "\(clouds) 🖕🏿"
            }
            if let rain: Double = notification.userInfo?["rain"] as? Double{
            currentRain = rain
            }
            currentTemp = temp - kelvin
            currentLocation = location
        }
        
        NotificationCenter.default.post(name     : Notification.Name.weatherProcessed,
                                        object   : nil,
                                        userInfo : nil)
    }
}


extension WeatherManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //Статус авторизации
        switch status {
        //Если он не определён (то есть ни одного запроса на авторизацию не было, то попросим базовую авторизацию)
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        //Если она ограничена или запрещена, то уведомим об отключении
        case .restricted, .denied:
            print("Отключаем локацию")
        //Если авторизация базовая, то попросим предоставить полную
        case .authorizedWhenInUse:
            print("Включаем базовые функции")
            manager.requestAlwaysAuthorization()
            manager.requestLocation()
        //Хи-хи
        case .authorizedAlways:
            manager.requestLocation()
            print("Теперь мы знаем, с кем Вы трахаетесь")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])  {
        
        for location in locations {
            let coordinate = location.coordinate
            NSLog("Широта \(coordinate.latitude), долгота \(coordinate.longitude)\n")
            WeatherModel.shared.getCurrentWether(longitude: coordinate.longitude, latitude: coordinate.latitude)
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //Если ошибку можно превратить в ошибку геопоозии, то сделаем это
        guard let locationError = error as? CLError else {
            //Иначе выведем как есть
            print(error)
            return
        }
        
        //Если получилось, то можно получить локализованное описание ошибки
        NSLog(locationError.localizedDescription)
    }
    
}
