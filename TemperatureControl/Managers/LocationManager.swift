//
//  LocationManager.swift
//  TemperatureControl
//
//  Copyright © 2019 Андрей Мироненко. All rights reserved.
//

//import Foundation
//import CoreLocation
//
// class LocationManager: NSObject, CLLocationManagerDelegate {
//    static let locManager = LocationManager()
//    let locationManager = CLLocationManager()
//
//
//    override init() {
//        super.init()
//        // Ask for Authorisation from the User.
//        self.locationManager.requestAlwaysAuthorization()
//
//        // For use in foreground
//        self.locationManager.requestWhenInUseAuthorization()
//
//        locationManager.delegate = self
//        locationManager.startUpdatingLocation()
//    }
//
//
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
//        print("locations = \(locValue.latitude) \(locValue.longitude)")
//    }
//}



import UIKit
import CoreLocation
import Foundation

class LocationManager: NSObject {
    
    static let shared = LocationManager()

    
    let manager: CLLocationManager = {
        let locationManager = CLLocationManager()
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        //Точность измерения самой системой - чем она лучше, тем больше энергии потребляеет приложение. Задаётся через набор k-констант. Старайтесь использовать ту точность, что реально важна для приложения
        locationManager.distanceFilter = 10
        //Свойство отвечает за фильтр дистанции - величину, лишь при изменении на которую будет срабатывать изменение локации
        
        locationManager.pausesLocationUpdatesAutomatically = true
        //Позволяет системе автоматически останавливать обновление локации для балансировщика энергии
        locationManager.activityType = .fitness
        //Через это свойство Вы можете указать тип действий, для которого используется геопозиция, это позволит системе лучше обрабатывать балансировку геопозиции
        locationManager.showsBackgroundLocationIndicator = true
        //С помощью этого свойства мы решаем, показывать или нет значок геопозиции для работы в фоновом режиме
        return locationManager
    }()
    
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //Статус авторизации
        switch status {
        //Если он не определён (то есть ни одного запроса на авторизацию не было, то попросим базовую авторизацию)
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        //Если она ограничена или запрещена, то уведомим об отключении
        case .restricted, .denied:
            print("Отключаем локацию")
        //Если авторизация базовая, то попросим предоставить полную
        case .authorizedWhenInUse:
            print("Включаем базовые функции")
            manager.requestAlwaysAuthorization()
            manager.requestLocation()
        //Хи-хи
        case .authorizedAlways:
            manager.requestLocation()
            print("Теперь мы знаем, с кем Вы трахаетесь")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])  {
        
        for location in locations {
            let coordinate = location.coordinate
            NSLog("Широта \(coordinate.latitude), долгота \(coordinate.longitude)\n")
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //Если ошибку можно превратить в ошибку геопоозии, то сделаем это
        guard let locationError = error as? CLError else {
            //Иначе выведем как есть
            print(error)
            return
        }
        
        //Если получилось, то можно получить локализованное описание ошибки
        NSLog(locationError.localizedDescription)
    }
}
