//
//  SessionManager.swift
//  TemperatureControl
//
//  Copyright © 2019 Андрей Мироненко. All rights reserved.
//

import Alamofire
import Foundation

class SessionManager {
    
    // MARK: Static Properties
    
    static let shared = SessionManager()
    
    // MARK: Public Properties
    
    let sessionManager: Alamofire.SessionManager
    
    // MARK: Public Methods
    
    init() {
        // Get the default headers
        var headers = Alamofire.SessionManager.defaultHTTPHeaders

        // Change default headers
        headers["Accept"] = "application/json"

        // Create a custom session configuration
        let configuration = URLSessionConfiguration.default

        // Add the headers
        configuration.httpAdditionalHeaders = headers

        // Create a session manager with the configuration
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
}
